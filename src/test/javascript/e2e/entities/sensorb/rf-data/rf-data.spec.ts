import { browser, element, by, protractor } from 'protractor';

import NavBarPage from './../../../page-objects/navbar-page';
import SignInPage from './../../../page-objects/signin-page';
import RFDataComponentsPage, { RFDataDeleteDialog } from './rf-data.page-object';
import RFDataUpdatePage from './rf-data-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../../util/utils';

const expect = chai.expect;

describe('RFData e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let rFDataComponentsPage: RFDataComponentsPage;
  let rFDataUpdatePage: RFDataUpdatePage;
  let rFDataDeleteDialog: RFDataDeleteDialog;
  let beforeRecordsCount = 0;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.loginWithOAuth('admin', 'admin');
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  it('should load RFData', async () => {
    await navBarPage.getEntityPage('rf-data');
    rFDataComponentsPage = new RFDataComponentsPage();
    expect(await rFDataComponentsPage.title.getText()).to.match(/RF Data/);

    expect(await rFDataComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilAnyDisplayed([rFDataComponentsPage.noRecords, rFDataComponentsPage.table]);

    beforeRecordsCount = (await isVisible(rFDataComponentsPage.noRecords)) ? 0 : await getRecordsCount(rFDataComponentsPage.table);
  });

  it('should load create RFData page', async () => {
    await rFDataComponentsPage.createButton.click();
    rFDataUpdatePage = new RFDataUpdatePage();
    expect(await rFDataUpdatePage.getPageTitle().getAttribute('id')).to.match(/webApp.sensorbRFData.home.createOrEditLabel/);
    await rFDataUpdatePage.cancel();
  });

  it('should create and save RFData', async () => {
    await rFDataComponentsPage.createButton.click();
    await rFDataUpdatePage.setRssiInput('5');
    expect(await rFDataUpdatePage.getRssiInput()).to.eq('5');
    await rFDataUpdatePage.setAltitudeInput('5');
    expect(await rFDataUpdatePage.getAltitudeInput()).to.eq('5');
    await rFDataUpdatePage.setMiddleFrequencyInput('5');
    expect(await rFDataUpdatePage.getMiddleFrequencyInput()).to.eq('5');
    await rFDataUpdatePage.setDateInput('01/01/2001' + protractor.Key.TAB + '02:30AM');
    expect(await rFDataUpdatePage.getDateInput()).to.contain('2001-01-01T02:30');
    await waitUntilDisplayed(rFDataUpdatePage.saveButton);
    await rFDataUpdatePage.save();
    await waitUntilHidden(rFDataUpdatePage.saveButton);
    expect(await isVisible(rFDataUpdatePage.saveButton)).to.be.false;

    expect(await rFDataComponentsPage.createButton.isEnabled()).to.be.true;

    await waitUntilDisplayed(rFDataComponentsPage.table);

    await waitUntilCount(rFDataComponentsPage.records, beforeRecordsCount + 1);
    expect(await rFDataComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);
  });

  it('should delete last RFData', async () => {
    const deleteButton = rFDataComponentsPage.getDeleteButton(rFDataComponentsPage.records.last());
    await click(deleteButton);

    rFDataDeleteDialog = new RFDataDeleteDialog();
    await waitUntilDisplayed(rFDataDeleteDialog.deleteModal);
    expect(await rFDataDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/webApp.sensorbRFData.delete.question/);
    await rFDataDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(rFDataDeleteDialog.deleteModal);

    expect(await isVisible(rFDataDeleteDialog.deleteModal)).to.be.false;

    await waitUntilAnyDisplayed([rFDataComponentsPage.noRecords, rFDataComponentsPage.table]);

    const afterCount = (await isVisible(rFDataComponentsPage.noRecords)) ? 0 : await getRecordsCount(rFDataComponentsPage.table);
    expect(afterCount).to.eq(beforeRecordsCount);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
