import { element, by, ElementFinder } from 'protractor';

export default class RFDataUpdatePage {
  pageTitle: ElementFinder = element(by.id('webApp.sensorbRFData.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  rssiInput: ElementFinder = element(by.css('input#rf-data-rssi'));
  altitudeInput: ElementFinder = element(by.css('input#rf-data-altitude'));
  middleFrequencyInput: ElementFinder = element(by.css('input#rf-data-middleFrequency'));
  dateInput: ElementFinder = element(by.css('input#rf-data-date'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setRssiInput(rssi) {
    await this.rssiInput.sendKeys(rssi);
  }

  async getRssiInput() {
    return this.rssiInput.getAttribute('value');
  }

  async setAltitudeInput(altitude) {
    await this.altitudeInput.sendKeys(altitude);
  }

  async getAltitudeInput() {
    return this.altitudeInput.getAttribute('value');
  }

  async setMiddleFrequencyInput(middleFrequency) {
    await this.middleFrequencyInput.sendKeys(middleFrequency);
  }

  async getMiddleFrequencyInput() {
    return this.middleFrequencyInput.getAttribute('value');
  }

  async setDateInput(date) {
    await this.dateInput.sendKeys(date);
  }

  async getDateInput() {
    return this.dateInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
