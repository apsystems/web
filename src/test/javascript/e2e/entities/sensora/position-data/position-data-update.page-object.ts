import { element, by, ElementFinder } from 'protractor';

export default class PositionDataUpdatePage {
  pageTitle: ElementFinder = element(by.id('webApp.sensoraPositionData.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  latInput: ElementFinder = element(by.css('input#position-data-lat'));
  lonInput: ElementFinder = element(by.css('input#position-data-lon'));
  altitudeInput: ElementFinder = element(by.css('input#position-data-altitude'));
  dateInput: ElementFinder = element(by.css('input#position-data-date'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setLatInput(lat) {
    await this.latInput.sendKeys(lat);
  }

  async getLatInput() {
    return this.latInput.getAttribute('value');
  }

  async setLonInput(lon) {
    await this.lonInput.sendKeys(lon);
  }

  async getLonInput() {
    return this.lonInput.getAttribute('value');
  }

  async setAltitudeInput(altitude) {
    await this.altitudeInput.sendKeys(altitude);
  }

  async getAltitudeInput() {
    return this.altitudeInput.getAttribute('value');
  }

  async setDateInput(date) {
    await this.dateInput.sendKeys(date);
  }

  async getDateInput() {
    return this.dateInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
