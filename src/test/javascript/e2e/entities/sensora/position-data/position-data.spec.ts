import { browser, element, by, protractor } from 'protractor';

import NavBarPage from './../../../page-objects/navbar-page';
import SignInPage from './../../../page-objects/signin-page';
import PositionDataComponentsPage, { PositionDataDeleteDialog } from './position-data.page-object';
import PositionDataUpdatePage from './position-data-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../../util/utils';

const expect = chai.expect;

describe('PositionData e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let positionDataComponentsPage: PositionDataComponentsPage;
  let positionDataUpdatePage: PositionDataUpdatePage;
  let positionDataDeleteDialog: PositionDataDeleteDialog;
  let beforeRecordsCount = 0;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.loginWithOAuth('admin', 'admin');
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  it('should load PositionData', async () => {
    await navBarPage.getEntityPage('position-data');
    positionDataComponentsPage = new PositionDataComponentsPage();
    expect(await positionDataComponentsPage.title.getText()).to.match(/Position Data/);

    expect(await positionDataComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilAnyDisplayed([positionDataComponentsPage.noRecords, positionDataComponentsPage.table]);

    beforeRecordsCount = (await isVisible(positionDataComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(positionDataComponentsPage.table);
  });

  it('should load create PositionData page', async () => {
    await positionDataComponentsPage.createButton.click();
    positionDataUpdatePage = new PositionDataUpdatePage();
    expect(await positionDataUpdatePage.getPageTitle().getAttribute('id')).to.match(/webApp.sensoraPositionData.home.createOrEditLabel/);
    await positionDataUpdatePage.cancel();
  });

  it('should create and save PositionData', async () => {
    await positionDataComponentsPage.createButton.click();
    await positionDataUpdatePage.setLatInput('5');
    expect(await positionDataUpdatePage.getLatInput()).to.eq('5');
    await positionDataUpdatePage.setLonInput('5');
    expect(await positionDataUpdatePage.getLonInput()).to.eq('5');
    await positionDataUpdatePage.setAltitudeInput('5');
    expect(await positionDataUpdatePage.getAltitudeInput()).to.eq('5');
    await positionDataUpdatePage.setDateInput('01/01/2001' + protractor.Key.TAB + '02:30AM');
    expect(await positionDataUpdatePage.getDateInput()).to.contain('2001-01-01T02:30');
    await waitUntilDisplayed(positionDataUpdatePage.saveButton);
    await positionDataUpdatePage.save();
    await waitUntilHidden(positionDataUpdatePage.saveButton);
    expect(await isVisible(positionDataUpdatePage.saveButton)).to.be.false;

    expect(await positionDataComponentsPage.createButton.isEnabled()).to.be.true;

    await waitUntilDisplayed(positionDataComponentsPage.table);

    await waitUntilCount(positionDataComponentsPage.records, beforeRecordsCount + 1);
    expect(await positionDataComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);
  });

  it('should delete last PositionData', async () => {
    const deleteButton = positionDataComponentsPage.getDeleteButton(positionDataComponentsPage.records.last());
    await click(deleteButton);

    positionDataDeleteDialog = new PositionDataDeleteDialog();
    await waitUntilDisplayed(positionDataDeleteDialog.deleteModal);
    expect(await positionDataDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/webApp.sensoraPositionData.delete.question/);
    await positionDataDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(positionDataDeleteDialog.deleteModal);

    expect(await isVisible(positionDataDeleteDialog.deleteModal)).to.be.false;

    await waitUntilAnyDisplayed([positionDataComponentsPage.noRecords, positionDataComponentsPage.table]);

    const afterCount = (await isVisible(positionDataComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(positionDataComponentsPage.table);
    expect(afterCount).to.eq(beforeRecordsCount);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
