import { Moment } from 'moment';

export interface IRFData {
  id?: string;
  rssi?: number;
  altitude?: number;
  middleFrequency?: number;
  date?: string;
}

export const defaultValue: Readonly<IRFData> = {};
