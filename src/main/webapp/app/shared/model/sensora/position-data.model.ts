import { Moment } from 'moment';

export interface IPositionData {
  id?: string;
  lat?: number;
  lon?: number;
  altitude?: number;
  date?: string;
}

export const defaultValue: Readonly<IPositionData> = {};
