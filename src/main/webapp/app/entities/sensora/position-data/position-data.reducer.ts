import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IPositionData, defaultValue } from 'app/shared/model/sensora/position-data.model';

export const ACTION_TYPES = {
  FETCH_POSITIONDATA_LIST: 'positionData/FETCH_POSITIONDATA_LIST',
  FETCH_POSITIONDATA: 'positionData/FETCH_POSITIONDATA',
  CREATE_POSITIONDATA: 'positionData/CREATE_POSITIONDATA',
  UPDATE_POSITIONDATA: 'positionData/UPDATE_POSITIONDATA',
  DELETE_POSITIONDATA: 'positionData/DELETE_POSITIONDATA',
  RESET: 'positionData/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPositionData>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type PositionDataState = Readonly<typeof initialState>;

// Reducer

export default (state: PositionDataState = initialState, action): PositionDataState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_POSITIONDATA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_POSITIONDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_POSITIONDATA):
    case REQUEST(ACTION_TYPES.UPDATE_POSITIONDATA):
    case REQUEST(ACTION_TYPES.DELETE_POSITIONDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_POSITIONDATA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_POSITIONDATA):
    case FAILURE(ACTION_TYPES.CREATE_POSITIONDATA):
    case FAILURE(ACTION_TYPES.UPDATE_POSITIONDATA):
    case FAILURE(ACTION_TYPES.DELETE_POSITIONDATA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_POSITIONDATA_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_POSITIONDATA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_POSITIONDATA):
    case SUCCESS(ACTION_TYPES.UPDATE_POSITIONDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_POSITIONDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/sensora/api/position-data';

// Actions

export const getEntities: ICrudGetAllAction<IPositionData> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_POSITIONDATA_LIST,
    payload: axios.get<IPositionData>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IPositionData> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_POSITIONDATA,
    payload: axios.get<IPositionData>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IPositionData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_POSITIONDATA,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IPositionData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_POSITIONDATA,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPositionData> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_POSITIONDATA,
    payload: axios.delete(requestUrl),
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
