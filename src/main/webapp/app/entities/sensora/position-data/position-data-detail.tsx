import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './position-data.reducer';
import { IPositionData } from 'app/shared/model/sensora/position-data.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPositionDataDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PositionDataDetail = (props: IPositionDataDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { positionDataEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="webApp.sensoraPositionData.detail.title">PositionData</Translate> [<b>{positionDataEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="lat">
              <Translate contentKey="webApp.sensoraPositionData.lat">Lat</Translate>
            </span>
          </dt>
          <dd>{positionDataEntity.lat}</dd>
          <dt>
            <span id="lon">
              <Translate contentKey="webApp.sensoraPositionData.lon">Lon</Translate>
            </span>
          </dt>
          <dd>{positionDataEntity.lon}</dd>
          <dt>
            <span id="altitude">
              <Translate contentKey="webApp.sensoraPositionData.altitude">Altitude</Translate>
            </span>
          </dt>
          <dd>{positionDataEntity.altitude}</dd>
          <dt>
            <span id="date">
              <Translate contentKey="webApp.sensoraPositionData.date">Date</Translate>
            </span>
          </dt>
          <dd>{positionDataEntity.date ? <TextFormat value={positionDataEntity.date} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
        </dl>
        <Button tag={Link} to="/position-data" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/position-data/${positionDataEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ positionData }: IRootState) => ({
  positionDataEntity: positionData.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PositionDataDetail);
