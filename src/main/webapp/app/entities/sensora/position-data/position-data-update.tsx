import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './position-data.reducer';
import { IPositionData } from 'app/shared/model/sensora/position-data.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPositionDataUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PositionDataUpdate = (props: IPositionDataUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { positionDataEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/position-data' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.date = convertDateTimeToServer(values.date);

    if (errors.length === 0) {
      const entity = {
        ...positionDataEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="webApp.sensoraPositionData.home.createOrEditLabel">
            <Translate contentKey="webApp.sensoraPositionData.home.createOrEditLabel">Create or edit a PositionData</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : positionDataEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="position-data-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="position-data-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="latLabel" for="position-data-lat">
                  <Translate contentKey="webApp.sensoraPositionData.lat">Lat</Translate>
                </Label>
                <AvField id="position-data-lat" type="string" className="form-control" name="lat" />
              </AvGroup>
              <AvGroup>
                <Label id="lonLabel" for="position-data-lon">
                  <Translate contentKey="webApp.sensoraPositionData.lon">Lon</Translate>
                </Label>
                <AvField id="position-data-lon" type="string" className="form-control" name="lon" />
              </AvGroup>
              <AvGroup>
                <Label id="altitudeLabel" for="position-data-altitude">
                  <Translate contentKey="webApp.sensoraPositionData.altitude">Altitude</Translate>
                </Label>
                <AvField id="position-data-altitude" type="string" className="form-control" name="altitude" />
              </AvGroup>
              <AvGroup>
                <Label id="dateLabel" for="position-data-date">
                  <Translate contentKey="webApp.sensoraPositionData.date">Date</Translate>
                </Label>
                <AvInput
                  id="position-data-date"
                  type="datetime-local"
                  className="form-control"
                  name="date"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.positionDataEntity.date)}
                />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/position-data" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  positionDataEntity: storeState.positionData.entity,
  loading: storeState.positionData.loading,
  updating: storeState.positionData.updating,
  updateSuccess: storeState.positionData.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PositionDataUpdate);
