import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import PositionData from './position-data';
import PositionDataDetail from './position-data-detail';
import PositionDataUpdate from './position-data-update';
import PositionDataDeleteDialog from './position-data-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={PositionDataDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PositionDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PositionDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PositionDataDetail} />
      <ErrorBoundaryRoute path={match.url} component={PositionData} />
    </Switch>
  </>
);

export default Routes;
