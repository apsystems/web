import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './rf-data.reducer';
import { IRFData } from 'app/shared/model/sensorb/rf-data.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IRFDataDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const RFDataDetail = (props: IRFDataDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { rFDataEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="webApp.sensorbRFData.detail.title">RFData</Translate> [<b>{rFDataEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="rssi">
              <Translate contentKey="webApp.sensorbRFData.rssi">Rssi</Translate>
            </span>
          </dt>
          <dd>{rFDataEntity.rssi}</dd>
          <dt>
            <span id="altitude">
              <Translate contentKey="webApp.sensorbRFData.altitude">Altitude</Translate>
            </span>
          </dt>
          <dd>{rFDataEntity.altitude}</dd>
          <dt>
            <span id="middleFrequency">
              <Translate contentKey="webApp.sensorbRFData.middleFrequency">Middle Frequency</Translate>
            </span>
          </dt>
          <dd>{rFDataEntity.middleFrequency}</dd>
          <dt>
            <span id="date">
              <Translate contentKey="webApp.sensorbRFData.date">Date</Translate>
            </span>
          </dt>
          <dd>{rFDataEntity.date ? <TextFormat value={rFDataEntity.date} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
        </dl>
        <Button tag={Link} to="/rf-data" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/rf-data/${rFDataEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ rFData }: IRootState) => ({
  rFDataEntity: rFData.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(RFDataDetail);
