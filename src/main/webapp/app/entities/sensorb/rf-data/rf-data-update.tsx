import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './rf-data.reducer';
import { IRFData } from 'app/shared/model/sensorb/rf-data.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IRFDataUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const RFDataUpdate = (props: IRFDataUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { rFDataEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/rf-data' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.date = convertDateTimeToServer(values.date);

    if (errors.length === 0) {
      const entity = {
        ...rFDataEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="webApp.sensorbRFData.home.createOrEditLabel">
            <Translate contentKey="webApp.sensorbRFData.home.createOrEditLabel">Create or edit a RFData</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : rFDataEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="rf-data-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="rf-data-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="rssiLabel" for="rf-data-rssi">
                  <Translate contentKey="webApp.sensorbRFData.rssi">Rssi</Translate>
                </Label>
                <AvField id="rf-data-rssi" type="string" className="form-control" name="rssi" />
              </AvGroup>
              <AvGroup>
                <Label id="altitudeLabel" for="rf-data-altitude">
                  <Translate contentKey="webApp.sensorbRFData.altitude">Altitude</Translate>
                </Label>
                <AvField id="rf-data-altitude" type="string" className="form-control" name="altitude" />
              </AvGroup>
              <AvGroup>
                <Label id="middleFrequencyLabel" for="rf-data-middleFrequency">
                  <Translate contentKey="webApp.sensorbRFData.middleFrequency">Middle Frequency</Translate>
                </Label>
                <AvField id="rf-data-middleFrequency" type="string" className="form-control" name="middleFrequency" />
              </AvGroup>
              <AvGroup>
                <Label id="dateLabel" for="rf-data-date">
                  <Translate contentKey="webApp.sensorbRFData.date">Date</Translate>
                </Label>
                <AvInput
                  id="rf-data-date"
                  type="datetime-local"
                  className="form-control"
                  name="date"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.rFDataEntity.date)}
                />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/rf-data" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  rFDataEntity: storeState.rFData.entity,
  loading: storeState.rFData.loading,
  updating: storeState.rFData.updating,
  updateSuccess: storeState.rFData.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(RFDataUpdate);
