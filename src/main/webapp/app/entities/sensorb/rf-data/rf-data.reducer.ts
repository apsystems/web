import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IRFData, defaultValue } from 'app/shared/model/sensorb/rf-data.model';

export const ACTION_TYPES = {
  FETCH_RFDATA_LIST: 'rFData/FETCH_RFDATA_LIST',
  FETCH_RFDATA: 'rFData/FETCH_RFDATA',
  CREATE_RFDATA: 'rFData/CREATE_RFDATA',
  UPDATE_RFDATA: 'rFData/UPDATE_RFDATA',
  DELETE_RFDATA: 'rFData/DELETE_RFDATA',
  RESET: 'rFData/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IRFData>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type RFDataState = Readonly<typeof initialState>;

// Reducer

export default (state: RFDataState = initialState, action): RFDataState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_RFDATA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_RFDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_RFDATA):
    case REQUEST(ACTION_TYPES.UPDATE_RFDATA):
    case REQUEST(ACTION_TYPES.DELETE_RFDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_RFDATA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_RFDATA):
    case FAILURE(ACTION_TYPES.CREATE_RFDATA):
    case FAILURE(ACTION_TYPES.UPDATE_RFDATA):
    case FAILURE(ACTION_TYPES.DELETE_RFDATA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_RFDATA_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_RFDATA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_RFDATA):
    case SUCCESS(ACTION_TYPES.UPDATE_RFDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_RFDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/sensorb/api/rf-data';

// Actions

export const getEntities: ICrudGetAllAction<IRFData> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_RFDATA_LIST,
    payload: axios.get<IRFData>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IRFData> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_RFDATA,
    payload: axios.get<IRFData>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IRFData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_RFDATA,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IRFData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_RFDATA,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IRFData> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_RFDATA,
    payload: axios.delete(requestUrl),
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
