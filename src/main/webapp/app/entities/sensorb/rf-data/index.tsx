import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import RFData from './rf-data';
import RFDataDetail from './rf-data-detail';
import RFDataUpdate from './rf-data-update';
import RFDataDeleteDialog from './rf-data-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={RFDataDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={RFDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={RFDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={RFDataDetail} />
      <ErrorBoundaryRoute path={match.url} component={RFData} />
    </Switch>
  </>
);

export default Routes;
